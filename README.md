# Hra Oběšenec
```bash
    +---+
    |   |
    |   |
    0   |
   /|\  |
    |   |
   / \  |
        |
==========
```

## Popis hry

Hra je implementována pomocí dvou vláken. První vlákno je zodpovědné za zobrazení samotné hry, zatímco druhé vlákno se stará o příjem vstupu od hráče.

Cílem hry je uhádnout skryté slovo. Hráč má k dispozici 10 pokusů a časový limit 20 sekund. Pokud hráč nedokáže uhodnout slovo do 20 sekund nebo vyčerpá všech 10 pokusů, prohrává. Za každé správně uhádnuté písmeno hráč získává 5 sekund navíc na časové úrovni.

Hra může být kdykoliv ukončena klávesou '0'.

## Kompilace programu
```bash
cmake -Bcmake-build-debug -H. -DCMAKE_BUILD_TYPE=Debug
cmake --build cmake-build-debug
```

## Příklady spuštění programu
### Spustit hru
```bash
./cmake-build-debug/pcc_hangman
```
### Zobrazit nápovědu
```bash
./cmake-build-debug/pcc_hangman --help
```
Po zobrazení pravidel, bude hráč dotázán, zda-li chce spustit či ukončit hru.
```
+---------------------------------------------------+
|               Vítej ve hře Oběšenec.              |
| Pravidla hry jsou jasná a jednoduchá:             |
| 1. Cílem je uhádnout tajné slovo.                 |
| 2. K dispozici je jen a pouze 10 pokusů           |
| 3. Časomíra začíná na 30 sekundách.               |
| 4. Za každé uhádnuté písmeno se příčítá 5 sekund. |
| 5. Hru lze kdykoli ukončit klávesou '0'.          |
| Hodně štěstí :-)                                  |
+---------------------------------------------------+
Napiš 'start' pro začátek hry nebo jakoukoli klávesu pro ukončení: 

```

## Testování
### Technika

Hra byla testována kompletně `ručně`, které díky rozdělení každé malé části do určitých funkcí bylo velmi přívětivé. Testováno bylo zda-li se opravdu po zadání všech správných písmen či vypršení herního času hra opravdu ukončí.

### Nástroje

Převážně jsem používal nástroj `Valgrind` pro detekci paměťových chyb a úniků paměti.

## Nedostatky
- Z důvodu neimplementovaného `non-blocking` inputu je v případě výhry či prohry třeba zadat jakýkoli znak, aby se program ukončil
- Při použití `system("clear")` se terminal nevymaže, ale pouze posune

## Finální slovo
Implementace této semestrální práce mě nad má očekávání velmi bavila, jelikož jsem uplatnil většinu konceptů a dovedností nabitých z cvičení. Rozhodně to bylo velmi příjemné zpestření oproti ostatním semestrálním pracím tento semestr.

Doufám, že výsledný program splňuje všechny požadavky a těším se na zpětnou vazbu. 
