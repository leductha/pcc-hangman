#include "main.hpp"


void printHelp() {
    cout << "+---------------------------------------------------+" << endl;
    cout << "|               Vítej ve hře Oběšenec.              |" << endl;
    cout << "| Pravidla hry jsou jasná a jednoduchá:             |" << endl;
    cout << "| 1. Cílem je uhádnout tajné slovo.                 |" << endl;
    cout << "| 2. K dispozici je jen a pouze 10 pokusů           |" << endl;
    cout << "| 3. Časomíra začíná na 30 sekundách.               |" << endl;
    cout << "| 4. Za každé uhádnuté písmeno se příčítá 5 sekund. |" << endl;
    cout << "| 5. Hru lze kdykoli ukončit klávesou '0'.          |" << endl;
    cout << "| Hodně štěstí :-)                                  |" << endl;
    cout << "+---------------------------------------------------+" << endl;
}

std::string generateRandomWord() {
    std::string s;

    std::vector<std::string> words = {
            "Pointer",
            "Template",
            "Compiler",
            "CMake",
            "iostream",
            "Namespace",
            "Copy",
            "Lambda",
            "Header",
            "Vector",
            "Friend",
            "Operator",
            "Overload",
            "Destructor",
            "Constructor",
            "Virtual",
            "Iterator",
            "Reference"
    };

    srand(static_cast<unsigned int>(std::time(0)));

    int randomIndex = rand() % words.size();

    s = words[randomIndex];

    std::transform(s.begin(), s.end(), s.begin(), ::toupper);

    return s;
}

    condition_variable cv;
    mutex mtx;
    string guesses;
    int timeLeft = 31;
    int wrongTries = 0;
    bool win = false;
    bool lost = false;
    bool ended = false;
    bool gameEnded = false;

    string wordToGuess = generateRandomWord();


    char input;
    void printMessage(string message, bool top = true, bool bottom = true)
    {
        int width = 33;
        if (top) {
            cout << "+";
            for (int i = 0; i < width; i++) {
                cout << "-";
            }
            cout << "+" << endl;
            cout << "|";
        } else {
            cout << "|";
        }

        // center the text
        bool boolean = true;
        for (int i = message.length(); i < width; i++) {
            if (boolean) {
                message = " " + message;
            } else {
                message = message + " ";
            }
            boolean = !boolean;
        }

        cout << message.c_str();

        if (bottom) {
            cout << "|" << endl;
            cout << "+";
            for (int i = 0; i < width; i++) {
                cout << "-";
            }
            cout << "+" << endl;
        } else {
            cout << "|" << endl;
        }
    }

    void drawHangman(int wrongGuesses = 0) {
        if (wrongGuesses >= 1) {
            printMessage("    +---+", false, false);
            printMessage("   |   |", false, false);
        }
        else {
            printMessage("    +---+", false, false);
        }

        if (wrongGuesses >= 2) {
            printMessage("   |   |", false, false);
        }
        else {
            printMessage("       |", false, false);
        }

        if (wrongGuesses >= 3) {
            printMessage("   0   |", false, false);
        }
        else {
            printMessage("       |", false, false);
        }

        if (wrongGuesses == 4) {
            printMessage("  /    |", false, false);
        }

        if (wrongGuesses == 5) {
            printMessage("  /|   |", false, false);
        }

        if (wrongGuesses >= 6) {
            printMessage("  /|\\  |", false, false);
        } else {
            printMessage("       |", false, false);
        }

        if (wrongGuesses >= 7) {
            printMessage("   |   |", false, false);
        } else {
            printMessage("       |", false, false);
        }

        if (wrongGuesses == 8) {
            printMessage("  /    |", false, false);
        }

        if (wrongGuesses >= 9) {
            printMessage("  / \\  |", false, false);
            printMessage("       |", false, false);
            printMessage("==========", false, false);
        } else if (wrongGuesses == 4 || wrongGuesses == 5 || wrongGuesses == 8) {
            printMessage("       |", false, false);
            printMessage("==========", false, false);
        }
        else {
            if (wrongGuesses == 0) {
                printMessage("       |", false, false);
            }
            printMessage("       |", false, false);
            printMessage("       |", false, false);
            printMessage("==========", false, false);
        }
    }

    void printLetters(string input, char from, char to) {
        string s;
        for (char i = from; i <= to; i++) {
            if (input.find(i) > input.length()) {
                s += i;
                s += " ";
            } else {
                s += "_ ";
            }
        }

        printMessage(s, false, false);
    }

    void printAlphabet(string taken) {
        printMessage("PISMENA K DISPOZICI");
        printLetters(taken, 'A', 'M');
        printLetters(taken, 'N', 'Z');
    }

    bool isSecretWord(string wordToGuess, string userGuesses) {
        bool won = true;
        string s;
        for (int i = 0; i < wordToGuess.length(); i++) {
            if (userGuesses.find(wordToGuess[i]) == string::npos) { // if in all guesses is not the word we want
                won = false;
                s += "_ ";
            } else {
                s += wordToGuess[i];
                s += " ";
            }
        }
        printMessage(s, false);
        return won;
    }

    int triesLeft(string wordToGuess, string userGuesses) {
        int error = 0;
        for (int i = 0; i < userGuesses.length(); i++) {
            if (wordToGuess.find(userGuesses[i]) == string::npos) {
                error++;
            }
        }
        return error;
    }


    void printYouWon() {
        cout << R"(
 ___      ___ ___    ___ ___  ___  ________  ___                  ___  ________  ___
|\  \    /  /|\  \  /  /|\  \|\  \|\   __  \|\  \                |\  \|\   ____\|\  \
\ \  \  /  / | \  \/  / | \  \\\  \ \  \|\  \ \  \               \ \  \ \  \___|\ \  \
 \ \  \/  / / \ \    / / \ \   __  \ \   _  _\ \  \            __ \ \  \ \_____  \ \  \
  \ \    / /   \/  /  /   \ \  \ \  \ \  \\  \\ \  \____      |\  \\_\  \|____|\  \ \  \
   \ \__/ /  __/  / /      \ \__\ \__\ \__\\ _\\ \_______\    \ \________\____\_\  \ \__\
    \|__|/  |\___/ /        \|__|\|__|\|__|\|__|\|_______|     \|________|\_________\|__|
            \|___|/                                                      \|_________|

)" << endl;

    }

    void printYouLost() {
        cout << R"(
 ________  ________  ________  ___  ___  ________  ________  ___                  ___  ________  ___
|\   __  \|\   __  \|\   __  \|\  \|\  \|\   __  \|\   __  \|\  \                |\  \|\   ____\|\  \
\ \  \|\  \ \  \|\  \ \  \|\  \ \  \\\  \ \  \|\  \ \  \|\  \ \  \               \ \  \ \  \___|\ \  \
 \ \   ____\ \   _  _\ \  \\\  \ \   __  \ \   _  _\ \   __  \ \  \            __ \ \  \ \_____  \ \  \
  \ \  \___|\ \  \\  \\ \  \\\  \ \  \ \  \ \  \\  \\ \  \ \  \ \  \____      |\  \\_\  \|____|\  \ \  \
   \ \__\    \ \__\\ _\\ \_______\ \__\ \__\ \__\\ _\\ \__\ \__\ \_______\    \ \________\____\_\  \ \__\
    \|__|     \|__|\|__|\|_______|\|__|\|__|\|__|\|__|\|__|\|__|\|_______|     \|________|\_________\|__|
                                                                                         \|_________|
)" << endl;
        cout << "Hledané slovo bylo: " << wordToGuess;
    }

    void inputThread() {
        while (true) {

            if (gameEnded) {
                system("clear");
                break;
            }

            cin >> input;
            if (input == '0') {
                system("clear");
                ended = true;
                break;
            }

            char upperInput = toupper(input);
            if (guesses.find(upperInput) == string::npos) {
                guesses += upperInput;
                if (wordToGuess.find(upperInput) != string::npos) {
                    timeLeft += 5;
                }
            }
        }
    }

    void update() {
        if (timeLeft == 0 || wrongTries >= 10) {
            lost = true;
            gameEnded = true;
            cv.notify_all();
        } else if (win) {
            win = true;
            gameEnded = true;
            cv.notify_all();
        }

    }

    void outputThread() {

        while (true) {
            if (input == '0' || gameEnded) {
                system("clear");
                break;
            }

            unique_lock<mutex> lock(mtx);
            if (cv.wait_for(lock, chrono::milliseconds(1000)) == cv_status::timeout) {
                system("clear");

                timeLeft--;
                wrongTries = triesLeft(wordToGuess, guesses);
                printMessage("OBESENEC");
                drawHangman(wrongTries);

                printAlphabet(guesses);

                printMessage("TAJNE SLOVO");

                win = isSecretWord(wordToGuess, guesses); // checks if the word was guessed

                cout << "Zbývající čas: " << timeLeft << endl;

                update();
            }
        }
        cv.notify_all();
    }

void printMenu() {
        system("clear");
        cout << R"(
              ____  ____  ______  _____ ______ _   _ ______ _____
             / __ \|  _ \|  ____|/ ____|  ____| \ | |  ____/ ____|
            | |  | | |_) | |__  | (___ | |__  |  \| | |__ | |
            | |  | |  _ <|  __|  \___ \|  __| | . ` |  __|| |
            | |__| | |_) | |____ ____) | |____| |\  | |___| |____
             \____/|____/|______|_____/|______|_| \_|______\_____|
        )" << endl;


    cout << R"(
                            Vítej ve hře Oběšenec!

                        Vyber si z následující nabídky:

                                S - Začátek hry

                                H - Nápověda

                                Cokoli - Konec
)";

    }

void startGame() {
    thread input(inputThread);
    thread output(outputThread);

    input.join();
    output.join();
}



int main(int argc, char *argv[]) {

    for (int i = 0; i < argc; ++i) {
        if (std::string(argv[i]) == "--help") {
            printHelp();
            char start;
            cout << "Napiš 'M' pro přesměrování do menu nebo cokoli pro ukončení: ";
            cin >> start;
            if (toupper(start) == 'M') {
                continue;
            } else {
                cout << "HRA BYLA UKONČENA" << endl;
                return 0;
            }
        }
    }

    printMenu();

    char userInput;
    cin >> userInput;

    switch (toupper(userInput)) {
        case 'S':
            startGame();
            break;
        case 'H':
            system("clear");
            printHelp();
            char start;
            cout << "Napiš 'S' pro začátek hry nebo jakoukoli klávesu pro ukončení: ";
            cin >> start;

            if (toupper(start) == 'S') {
                startGame();
            } else {
                gameEnded = true;
            }
            break;
        default:
            system("clear");
            cout << "HRA BYLA UKONČENA" << endl;

    }



    if (win) {
        printYouWon();
    } else if (lost) {
        printYouLost();
    } else if (ended) {
        cout << endl;
        cout << "HRA BYLA UKONČENA" << endl;
    }

    return 0;
}

