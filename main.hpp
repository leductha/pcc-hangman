#ifndef PCC_HANGMAN_MAIN_HPP
#define PCC_HANGMAN_MAIN_HPP



#include <iostream>
#include <thread>
#include <atomic>
#include <algorithm>
#include <cctype>
#include <condition_variable>
#include <mutex>

using namespace std;

void printHelp();
void printMenu();
void startGame();
void printYouWon();
void printYouLost();
void inputThread();
void outputThread();


#endif
